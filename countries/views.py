# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.shortcuts import render
from django.views import View

from .models import Group
from game.models import GameDay, PlayOffImage


class Timing(View):
    template_name = 'timing/timing_page.html'

    def get(self, request, *args, **kwargs):
        group_list = GameDay.objects.all()[:15]
        play_off_list = GameDay.objects.all()[15:]
        tabel_list = Group.objects.all()
        playoff_img = PlayOffImage.objects.all()
        today =  datetime.now()
        context = {
            "tabel_list": tabel_list,
            "group_list": group_list,
            "play_off_list": play_off_list,
            "today": today,
            "playoff_img": playoff_img
        }
        return render(request, self.template_name, context)
