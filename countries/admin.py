# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Group, Country


class GroupAdmin(admin.ModelAdmin):
    list_display = ["title"]
    list_display_links = ["title"]

    class Meta:
        model = Group


class CountryAdmin(admin.ModelAdmin):
    list_display = ["title"]
    list_display_links = ["title"]

    class Meta:
        model = Country


admin.site.register(Group, GroupAdmin)
admin.site.register(Country, CountryAdmin)
