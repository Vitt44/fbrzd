from django.conf.urls import url

from .views import Timing

urlpatterns = [
    url(r'^timing/$', Timing.as_view(), name="timing"),
    url(r'^timing/en/$', Timing.as_view(template_name='timing/timing_page_en.html'), name="timing_en"),
    url(r'^timing/ch/$', Timing.as_view(template_name='timing/timing_page_ch.html'), name="timing_ch"),
]
