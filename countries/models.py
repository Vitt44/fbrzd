# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Country(models.Model):
    title = models.CharField(max_length=300, verbose_name='Заголовок', db_index=True)
    title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    country_img = models.ImageField(blank=True, verbose_name='Флаг', upload_to='country_img')
    games = models.CharField(max_length=10, default='0', verbose_name='Всего игр')
    wins = models.CharField(max_length=10, default='0', verbose_name='Выигрышей')
    draws = models.CharField(max_length=10, default='0', verbose_name='Ничьих')
    defeats = models.CharField(max_length=10, default='0', verbose_name='Поражений')
    goals_scored = models.CharField(max_length=10, default='',blank=True, verbose_name='Забитых мячей')
    missed_goals = models.CharField(max_length=10, default='0', verbose_name='Пропущенных мячей')
    difference = models.CharField(max_length=10, default='0', verbose_name='+/-')
    score = models.CharField(max_length=10, default='0', verbose_name='Очков')

    class Meta:
        verbose_name = 'Страну'
        verbose_name_plural = 'Страны'

    # For Python 2, use __unicode__ too
    # For Python 3, use __str__ too
    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        try:
            this_record = Country.objects.get(id=self.id)
            if this_record.country_img != self.country_img:
                this_record.country_img.delete(save=False)
        except:
            pass
        super(Country, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.country_img.delete()
        super(Country, self).delete(*args, **kwargs)


class Group(models.Model):
    title = models.CharField(max_length=300, verbose_name='Заголовок', db_index=True)
    title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    country = models.ManyToManyField(Country, verbose_name='Страны')

    class Meta:
        verbose_name = 'Группу'
        verbose_name_plural = 'Группы'

    # For Python 2, use __unicode__ too
    # For Python 3, use __str__ too
    def __unicode__(self):
        return self.title
