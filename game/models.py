# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from countries.models import Country, Group


class GameCityGuide(models.Model):
    game_city = models.CharField(max_length=300, verbose_name='Город', db_index=True)
    game_city_en = models.CharField(blank=True, max_length=300, verbose_name='Город Eng')
    game_city_ch = models.CharField(blank=True, max_length=300, verbose_name='Город Chi')
    city_slug = models.SlugField(unique=True)

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def get_absolute_url(self):
        return reverse('city_page', kwargs={'slug': self.city_slug})

    def __unicode__(self):
        return self.game_city


class GameCityStadeon(models.Model):
    game_stadeon= models.CharField(max_length=300, verbose_name='Стадион', db_index=True)
    game_stadeon_en = models.CharField(blank=True, max_length=300, verbose_name='Стадион Eng')
    game_stadeon_ch = models.CharField(blank=True, max_length=300, verbose_name='Стадион Chi')
    city = models.ForeignKey(GameCityGuide, verbose_name='Город', default=1)

    class Meta:
        verbose_name = 'Стадион'
        verbose_name_plural = 'Стадионы'

    def __unicode__(self):
        return self.game_stadeon


class Game(models.Model):
    game_country_1 = models.ForeignKey(Country, blank=True, null=True, verbose_name='Страна 1', related_name='country_1')
    game_country_1_score = models.PositiveSmallIntegerField(default=0, verbose_name='Счет страны 1')
    game_prefix_1 = models.CharField(blank=True, max_length=30, verbose_name='Префикс игрока 1')
    game_country_2 = models.ForeignKey(Country, blank=True, null=True, verbose_name='Страна 2')
    game_country_2_score = models.PositiveSmallIntegerField(default=0, verbose_name='Счет страны 1')
    game_prefix_2 = models.CharField(blank=True, max_length=30, verbose_name='Префикс игрока 2')
    game_datetime = models.DateTimeField(verbose_name='Дата игры', db_index=True)
    game_group = models.ForeignKey(Group, blank=True, verbose_name='Группа', null=True)
    game_title = models.CharField(max_length=300, verbose_name='Заголовок')
    game_title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    game_title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    stadeon = models.ForeignKey(GameCityStadeon, verbose_name='Стадион')

    class Meta:
        verbose_name = 'Игру'
        verbose_name_plural = 'Игры'
        ordering = ["game_datetime"]

    def __unicode__(self):
        return self.game_title


class GameDay(models.Model):
    game_datetime_ru = models.CharField(max_length=100, verbose_name='Дата игры ru', default='14 июня, Четверг')
    game_datetime_en = models.CharField(blank=True, max_length=100, verbose_name='Дата игры en')
    game_datetime_ch = models.CharField(blank=True, max_length=100, verbose_name='Дата игры ch')
    game = models.ManyToManyField(Game, verbose_name='Игра')

    class Meta:
        verbose_name = 'Игровой день'
        verbose_name_plural = 'Игровые дни'

    def __unicode__(self):
        return self.game_datetime_ru


class PlayOffImage(models.Model):
    title = models.CharField(max_length=100, default='RU')
    img = models.ImageField(verbose_name='Изображение', upload_to='playoff_img',)

    class Meta:
        verbose_name = 'ПЛЕЙ-ОФФ Image'
        verbose_name_plural = 'ПЛЕЙ-ОФФ Images'

    def __unicode__(self):
        return 'Playoff image %s' % self.title

    def save(self, *args, **kwargs):
        try:
            this_record = PlayOffImage.objects.get(id=self.id)
            if this_record.play_off_img != self.play_off_img:
                this_record.play_off_img.delete(save=False)
        except:
            pass
        super(PlayOffImage, self).save(*args, **kwargs)
