# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import GameCityGuide, GameCityStadeon, Game, GameDay, PlayOffImage


class GameCityGuideAdmin(admin.ModelAdmin):
    list_display = ["game_city"]
    list_display_links = ["game_city"]
    prepopulated_fields = {'city_slug': ('game_city',)}

    class Meta:
        model = GameCityGuide


class GameCityStadeonAdmin(admin.ModelAdmin):
    list_display = ["game_stadeon"]
    list_display_links = ["game_stadeon"]

    class Meta:
        model = GameCityStadeon


class GameAdmin(admin.ModelAdmin):
    list_display = ["game_datetime", "game_title", "game_country_1", "game_country_2"]
    list_display_links = ["game_datetime"]
    list_filter = ('game_datetime',)
    date_hierarchy = 'game_datetime'
    ordering = ('-game_datetime',)
    search_fields = ["game_datetime", "game_country_1", "game_country_2"]

    class Meta:
        model = Game


class GameDayAdmin(admin.ModelAdmin):
    list_display = ["game_datetime_ru"]
    list_display_links = ["game_datetime_ru"]

    class Meta:
        model = GameDay


class PlayOffImageAdmin(admin.ModelAdmin):
    list_display = ["title", "img"]
    list_display_links = ["title"]
    exclude = ['title']
    actions = None

    class Meta:
        model = PlayOffImage


admin.site.register(GameCityGuide, GameCityGuideAdmin)
admin.site.register(GameCityStadeon, GameCityStadeonAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(GameDay, GameDayAdmin)
admin.site.register(PlayOffImage, PlayOffImageAdmin)
