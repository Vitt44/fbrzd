    $(document).ready(function(){ 
        PopUpHide();
    });
    function PopUpShow(){
        $("#popup1").show();
    }
    //Функция скрытия PopUp
    function PopUpHide(){
        $("#popup1").hide();
    }

    $(document).ready(function(){ 
        PopUpHide2();
    });
    function PopUpShow2(){
        $("#popup2").show();
    }
    //Функция скрытия PopUp
    function PopUpHide2(){
        $("#popup2").hide();
    }


    document.addEventListener('DOMContentLoaded', () => { 
  // This is the bare minimum JavaScript. You can opt to pass no arguments to setup.
  const player = new Plyr('.player');

  // Bind event listener
  function on(selector, type, callback) {
    document.querySelector(selector).addEventListener(type, callback, false);
  }

  // Play
  on('.js-play', 'click', () => { 
    player.play();
  });

  // Pause
  on('.js-pause', 'click', () => { 
    player.pause();
  });

  // Stop
  on('.js-stop', 'click', () => { 
    player.stop();
  });

  // Rewind
  on('.js-rewind', 'click', () => { 
    player.rewind();
  });

  // Forward
  on('.js-forward', 'click', () => { 
    player.forward();
  });
});

$(document).ready(function() {
    if(localStorage.getItem('popState') != 'shown'){
        $("#popup").delay(3000).fadeIn();
        setTimeout(function(){
        localStorage.setItem('popState','shown');
      }, 10000);
    }

    $('#popup-close').click(function(e) // You are clicking the close button
    {
    $('#popup').fadeOut(); // Now the pop up is hiden.
    });
    $('#popup').click(function(e) 
    {
    $('#popup').fadeOut(); 
    });
});