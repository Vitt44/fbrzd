from django import forms


class SuccessForm(forms.Form):
    accept = forms.BooleanField(widget=forms.CheckboxInput(), required=True)
