# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-28 09:55
from __future__ import unicode_literals

from django.db import migrations, models
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_rozanovarticle'),
    ]

    operations = [
        migrations.CreateModel(
            name='Story',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('title_en', models.CharField(blank=True, max_length=300, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a Eng')),
                ('title_ch', models.CharField(blank=True, max_length=300, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a Chi')),
                ('story_img', models.ImageField(blank=True, upload_to='story_img', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('story_tag', models.CharField(blank=True, max_length=300, verbose_name='\u0422\u044d\u0433')),
                ('story_tag_en', models.CharField(blank=True, max_length=300, verbose_name='\u0422\u044d\u0433 Eng')),
                ('story_tag_ch', models.CharField(blank=True, max_length=300, verbose_name='\u0422\u044d\u0433 Chi')),
                ('story_datetime', models.DateTimeField(db_index=True, verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
                ('story_author', models.CharField(blank=True, max_length=300, verbose_name='\u0410\u0432\u0442\u043e\u0440')),
                ('story_author_en', models.CharField(blank=True, max_length=300, verbose_name='\u0410\u0432\u0442\u043e\u0440 Eng')),
                ('story_author_ch', models.CharField(blank=True, max_length=300, verbose_name='\u0410\u0432\u0442\u043e\u0440 Chi')),
                ('story_content', redactor.fields.RedactorField(blank=True, verbose_name='\u041a\u043e\u043d\u0442\u0435\u043d\u0442')),
                ('story_content_en', redactor.fields.RedactorField(blank=True, verbose_name='\u041a\u043e\u043d\u0442\u0435\u043d\u0442 Eng')),
                ('story_content_ch', redactor.fields.RedactorField(blank=True, verbose_name='\u041a\u043e\u043d\u0442\u0435\u043d\u0442 Eng')),
                ('slug', models.SlugField(unique=True)),
            ],
            options={
                'ordering': ['-story_datetime'],
                'verbose_name': '\u0418\u0441\u0442\u043e\u0440\u0438\u044e',
                'verbose_name_plural': '\u0418\u0441\u0442\u043e\u0440\u0438\u0438',
            },
        ),
    ]
