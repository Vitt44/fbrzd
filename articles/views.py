# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import random
import os
import datetime
import json

from django.shortcuts import render, get_object_or_404
from django.views import View
from django.http import HttpResponseRedirect, Http404, JsonResponse, HttpResponse
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import loader

from game.models import GameCityGuide
from .models import ArticleGuide, RozanovArticle, Story, ArticleCategory, VideoMoment
from .forms import SuccessForm

# Signal to restart uwsgi
def get_restart(request):
    if request.user.is_superuser:
        try:
            f = open("get_restart.txt", "a")
            f.write("last restart at %s \n" % datetime.datetime.now())
            f.close()
            return HttpResponseRedirect('/')
        except:
            raise Http404()
    else:
        raise Http404()

# Проверка перед отправкой на поезда
def success_call(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = SuccessForm(request.POST)
            accept = form['accept']
            if accept:
                try:
                    f = open("success_to_upload.txt", "a")
                    f.write("push button at %s \n" % datetime.datetime.now())
                    f.close()
                except:
                    messages.error(request, 'Что-то пошло не так!')
                messages.success(request, "Сигнал отправлен!")
                return render(request, 'success.html')
        else:
            form = SuccessForm()
        return render(request, 'success.html', {'form': form})
    else:
        return HttpResponseRedirect('/')

# video СПЕЦПРОЕКТ ЛОКОМОТИВ
class GoldPath(View):
    template_name = 'videos/path_to_gold.html'

    def get(self, request, *args, **kwargs):
        online = ''
        if os.path.exists("is_online.py"):
             online += '1'
        else:
             online += '2'
        videos = VideoMoment.objects.filter(video_tag='Локомотив')
        context = {
        'videos': videos,
        'online': online
        }
        return render(request, self.template_name, context)

# video
class Video(View):
    template_name = 'videos/video.html'

    def get(self, request, *args, **kwargs):
        online = ''
        if os.path.exists("is_online.py"):
             online += '1'
        else:
             online += '2'
        videos = VideoMoment.objects.all()
        context = {
        'videos': videos,
        'online': online
        }
        return render(request, self.template_name, context)

# GUIDE
class CityPage(View):
    template_name = 'articles/guide/city_page.html'

    def get(self, request, slug, *args, **kwargs):
        cities = GameCityGuide.objects.all()
        city = get_object_or_404(GameCityGuide, city_slug=slug)
        articles = ArticleGuide.objects.filter(article_city=city)
        context = {"city": city, "articles": articles, "cities": cities}
        return render(request, self.template_name, context)


class GuidePage(View):
    template_name = 'articles/guide/guide_page.html'

    def get(self, request, slug, city, *args, **kwargs):
        article = get_object_or_404(ArticleGuide, slug=slug, article_city__city_slug=city)
        return render(request, self.template_name, {"article": article})

# ROZANOV
class RozanovList(View):
    template_name = 'articles/rozanov/rozanov_page.html'

    def get(self, request, *args, **kwargs):
        citations_rus = [
        "Во время ЧМ стараюсь стать ребенком",
        "Не всегда бывает, что ЧМ выигрывает лучшая команда",
        "Чемпионат — не только соревнование, но и смотр талантов"
        ]
        random.shuffle(citations_rus)
        c_rus = random.choice(citations_rus)
        citations_eng = [
        "For the World Cup, I’ll try to become a child again",
        "World champions are not always the best team of the tournament",
        "The World Cup is not just a competition, but a global talent show"
        ]
        random.shuffle(citations_eng)
        c_eng = random.choice(citations_eng)
        articles = RozanovArticle.objects.all()
        context = {
        "articles": articles,
        "citat_rus": c_rus,
        "citat_eng": c_eng
        }
        return render(request, self.template_name, context)

class RozanovPage(View):
    template_name = 'articles/rozanov/rozanov_article.html'

    def get(self, request, slug, *args, **kwargs):
        article = get_object_or_404(RozanovArticle, slug=slug)
        return render(request, self.template_name, {"article": article})

# stories (News, FIFA)
class News(View):
    template_name = 'articles/stories/story_list.html'
    id = 1
    header = 'НОВОСТИ ЧМ'
    ajax_url = '/load_articles/'

    def get(self, request, *args, **kwargs):
        paginate_by = 13
        online = ''
        if os.path.exists("is_online.py"):
             online += '1'
        else:
             online += '2'
        category = ArticleCategory.objects.get(id=self.id)
        story_list = Story.objects.filter(category=category)[:13]
        # paginator = Paginator(story_list, paginate_by)
        # page_number = request.GET.get("page")
        # try:
        #     page = paginator.page(page_number)
        # except PageNotAnInteger:
        # # If page is not an integer, show first page.
        #     page = paginator.page(1)
        # except EmptyPage:
        # # If page is out of range, show last existing page.
        #     page = paginator.page(paginator.num_pages)
        context = {
        "story_list": story_list,
        "header": self.header,
        "online": online,
        "ajax_url": self.ajax_url
        }
        return render(request, self.template_name, context)


def ch_story(request):
    header = '冠軍新聞'
    online = ''
    if os.path.exists("is_online.py"):
        online += '1'
    else:
        online += '2'
    category = ArticleCategory.objects.get(id=1)
    story_list = Story.objects.filter(category=category)
    context = {
        "story_list": story_list,
        "header": header,
        "online": online
        }
    return render(request, 'articles/stories/story_list_ch.html', context)

# Load Stories by ajax request
def load_articles(request):
  page = request.GET.get("page")
  category = ArticleCategory.objects.get(id=1)
  story_list = Story.objects.filter(category=category)
  paginator = Paginator(story_list, 13)
  try:
    posts = paginator.page(page)
  except PageNotAnInteger:
    posts = paginator.page(2)
  except EmptyPage:
    posts = paginator.page(paginator.num_pages)
  story_html = loader.render_to_string(
    "articles/stories/story.html",
    {"story_list": posts}
  )
  output_data = {
    "story_html": story_html,
    "has_next": posts.has_next()
  }
  return JsonResponse(output_data)

def load_articles_en(request):
  page = request.GET.get("page")
  category = ArticleCategory.objects.get(id=1)
  story_list = Story.objects.filter(category=category)
  paginator = Paginator(story_list, 13)
  try:
    posts = paginator.page(page)
  except PageNotAnInteger:
    posts = paginator.page(2)
  except EmptyPage:
    posts = paginator.page(paginator.num_pages)
  story_html = loader.render_to_string(
    "articles/stories/story_en.html",
    {"story_list": posts}
  )
  output_data = {
    "story_html": story_html,
    "has_next": posts.has_next()
  }
  return JsonResponse(output_data)

# Load FIFA by ajax request
def load_fifa(request):
  page = request.GET.get("page")
  category = ArticleCategory.objects.get(id=2)
  story_list = Story.objects.filter(category=category)
  paginator = Paginator(story_list, 13)
  try:
    posts = paginator.page(page)
  except PageNotAnInteger:
    posts = paginator.page(2)
  except EmptyPage:
    posts = paginator.page(paginator.num_pages)
  story_html = loader.render_to_string(
    "articles/stories/story.html",
    {"story_list": posts}
  )
  output_data = {
    "story_html": story_html,
    "has_next": posts.has_next()
  }
  return JsonResponse(output_data)

def load_fifa_en(request):
  page = request.GET.get("page")
  category = ArticleCategory.objects.get(id=2)
  story_list = Story.objects.filter(category=category)
  paginator = Paginator(story_list, 13)
  try:
    posts = paginator.page(page)
  except PageNotAnInteger:
    posts = paginator.page(2)
  except EmptyPage:
    posts = paginator.page(paginator.num_pages)
  story_html = loader.render_to_string(
    "articles/stories/story_en.html",
    {"story_list": posts}
  )
  output_data = {
    "story_html": story_html,
    "has_next": posts.has_next()
  }
  return JsonResponse(output_data)


class ArticlePage(View):
    template_name = 'articles/stories/story_page.html'

    def get(self, request, slug, *args, **kwargs):
        story = get_object_or_404(Story, slug=slug)
        return render(request, self.template_name, {"story": story})


# fans

class FansPage(View):
    template_name = 'for_fans/fans.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)
