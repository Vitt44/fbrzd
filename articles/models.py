# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from redactor.fields import RedactorField

from game.models import GameCityGuide


class ArticleGuide(models.Model):
    article_city = models.ForeignKey(GameCityGuide, verbose_name='Город')
    title = models.CharField(max_length=300, verbose_name='Заголовок')
    title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    preview = models.CharField(blank=True, max_length=300, verbose_name='Анонс',
                                help_text='анонс ограничен 300 символами')
    preview_en = models.CharField(blank=True, max_length=300, verbose_name='Анонс Eng')
    preview_ch = models.CharField(blank=True, max_length=300, verbose_name='Анонс Chi')
    article_img = models.ImageField(blank=True, verbose_name='Изображение', upload_to='story_img',
                                    help_text='соотношение сторон 5x3 (рекомендуется 800x480px)')
    article_tag = models.CharField(blank=True, max_length=300, verbose_name='Тэг')
    article_tag_en = models.CharField(blank=True, max_length=300, verbose_name='Тэг Eng')
    article_tag_ch = models.CharField(blank=True, max_length=300, verbose_name='Тэг Chi')
    article_datetime = models.DateTimeField(verbose_name='Дата публикации', db_index=True)
    article_author = models.CharField(blank=True, max_length=300, verbose_name='Автор')
    article_author_en = models.CharField(blank=True, max_length=300, verbose_name='Автор Eng')
    article_author_ch = models.CharField(blank=True, max_length=300, verbose_name='Автор Chi')
    article_content = RedactorField(blank=True, verbose_name='Контент')
    article_content_en = RedactorField(blank=True, verbose_name='Контент Eng')
    article_content_ch = RedactorField(blank=True, verbose_name='Контент Chi')
    slug = models.SlugField(unique=True)


    class Meta:
        verbose_name = 'Статью (Гид)'
        verbose_name_plural = 'Статьи (Гид)'
        ordering = ["-article_datetime"]

    # For Python 2, use __unicode__ too
    # For Python 3, use __str__ too
    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('guide_page', kwargs={'slug': self.slug, 'city': self.article_city.city_slug})

    def save(self, *args, **kwargs):
        try:
            this_record = ArticleGuide.objects.get(id=self.id)
            if this_record.article_img != self.article_img:
                this_record.article_img.delete(save=False)
        except:
            pass
        super(ArticleGuide, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.article_img.delete()
        super(ArticleGuide, self).delete(*args, **kwargs)


class RozanovArticle(models.Model):
    title = models.CharField(max_length=300, verbose_name='Заголовок')
    title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    preview = models.CharField(blank=True, max_length=300, verbose_name='Анонс',
                                help_text='анонс ограничен 300 символами')
    preview_en = models.CharField(blank=True, max_length=300, verbose_name='Анонс Eng')
    preview_ch = models.CharField(blank=True, max_length=300, verbose_name='Анонс Chi')
    article_img = models.ImageField(blank=True, verbose_name='Изображение', upload_to='story_img',
                                    help_text='соотношение сторон 5x3 (рекомендуется 800x480px)')
    article_tag = models.CharField(blank=True, max_length=300, verbose_name='Тэг')
    article_tag_en = models.CharField(blank=True, max_length=300, verbose_name='Тэг Eng')
    article_tag_ch = models.CharField(blank=True, max_length=300, verbose_name='Тэг Chi')
    article_datetime = models.DateTimeField(verbose_name='Дата публикации', db_index=True)
    article_content = RedactorField(blank=True, verbose_name='Контент')
    article_content_en = RedactorField(blank=True, verbose_name='Контент Eng')
    article_content_ch = RedactorField(blank=True, verbose_name='Контент Eng')
    slug = models.SlugField(unique=True)


    class Meta:
        verbose_name = 'Статью (Розанова)'
        verbose_name_plural = 'Статьи (Розанова)'
        ordering = ["-article_datetime"]

    # For Python 3, use __str__ too
    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('rozanov_article', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = RozanovArticle.objects.get(id=self.id)
            if this_record.article_img != self.article_img:
                this_record.article_img.delete(save=False)
        except:
            pass
        super(RozanovArticle, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.article_img.delete()
        super(RozanovArticle, self).delete(*args, **kwargs)


class ArticleCategory(models.Model):
    title = models.CharField(max_length=300, verbose_name='Заголовок')
    title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    slug = models.SlugField(unique=True)

    class Meta:
        verbose_name = 'Категорию'
        verbose_name_plural = 'Категории'

    # For Python 3, use __str__ too
    def __unicode__(self):
        return self.title

    # def get_absolute_url(self):
    #     return reverse('story_page', kwargs={'slug': self.slug})


class Story(models.Model):
    category = models.ForeignKey(ArticleCategory, verbose_name='Категория', blank=True, null=True)
    only_online = models.BooleanField(default=False, verbose_name='В поезде не показывать')
    title = models.CharField(max_length=300, verbose_name='Заголовок')
    title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    preview = models.CharField(blank=True, max_length=300, verbose_name='Анонс',
                                help_text='анонс ограничен 300 символами')
    preview_en = models.CharField(blank=True, max_length=300, verbose_name='Анонс Eng')
    preview_ch = models.CharField(blank=True, max_length=300, verbose_name='Анонс Chi')
    story_img = models.ImageField(blank=True, verbose_name='Изображение', upload_to='story_img',
                                help_text='соотношение сторон 5x3 (рекомендуется 800x480px)')
    story_tag = models.CharField(blank=True, max_length=300, verbose_name='Тэг')
    story_tag_en = models.CharField(blank=True, max_length=300, verbose_name='Тэг Eng')
    story_tag_ch = models.CharField(blank=True, max_length=300, verbose_name='Тэг Chi')
    story_datetime = models.DateTimeField(verbose_name='Дата публикации', db_index=True)
    story_author = models.CharField(blank=True, max_length=300, verbose_name='Автор')
    story_author_en = models.CharField(blank=True, max_length=300, verbose_name='Автор Eng')
    story_author_ch = models.CharField(blank=True, max_length=300, verbose_name='Автор Chi')
    story_content = RedactorField(blank=True, verbose_name='Контент')
    story_content_en = RedactorField(blank=True, verbose_name='Контент Eng')
    story_content_ch = RedactorField(blank=True, verbose_name='Контент Eng')
    slug = models.SlugField(unique=True)


    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ["-story_datetime"]

    # For Python 3, use __str__ too
    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('article_page', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = Story.objects.get(id=self.id)
            if this_record.story_img != self.story_img:
                this_record.story_img.delete(save=False)
        except:
            pass
        super(Story, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.story_img.delete()
        super(Story, self).delete(*args, **kwargs)


class VideoMoment(models.Model):
    title = models.CharField(max_length=300, verbose_name='Заголовок')
    title_en = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Eng')
    title_ch = models.CharField(blank=True, max_length=300, verbose_name='Заголовок Chi')
    video_img = models.ImageField(verbose_name='Титульное изображение', upload_to='video_img',
                                help_text='соотношение сторон 5x3 (рекомендуется 800x480px)')
    video_file = models.FileField(blank=True, verbose_name='Видео файл', upload_to='videos')
    youtube_url = models.URLField(blank=True, max_length=1000, verbose_name='Youtube src')
    video_tag = models.CharField(blank=True, max_length=300, verbose_name='Тэг')
    video_tag_en = models.CharField(blank=True, max_length=300, verbose_name='Тэг Eng')
    video_tag_ch = models.CharField(blank=True, max_length=300, verbose_name='Тэг Chi')
    video_datetime = models.DateTimeField(verbose_name='Дата публикации', db_index=True)


    class Meta:
        verbose_name = 'Видео'
        verbose_name_plural = 'Видео'
        ordering = ["-video_datetime"]

    # For Python 3, use __str__ too
    def __unicode__(self):
        return self.title


    def save(self, *args, **kwargs):
        try:
            this_record = VideoMoment.objects.get(id=self.id)
            if this_record.video_file != self.video_file:
                this_record.video_file.delete(save=False)
            if this_record.video_img != self.video_img:
                this_record.video_img.delete(save=False)
        except:
            pass
        super(VideoMoment, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.video_img.delete()
        self.video_file.delete()
        super(VideoMoment, self).delete(*args, **kwargs)
