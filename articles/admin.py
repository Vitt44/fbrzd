# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import ArticleGuide, RozanovArticle, Story, ArticleCategory, VideoMoment


class ArticleGuideAdmin(admin.ModelAdmin):
    list_display = ["title", "article_city", "article_datetime", "article_author"]
    list_display_links = ["title"]
    list_filter = ('article_datetime',)
    date_hierarchy = 'article_datetime'
    # ordering = ('-article_datetime',)
    search_fields = ["title", "article_author"]
    prepopulated_fields = {'slug': ('title',)}

    class Meta:
        model = ArticleGuide


class RozanovArticleAdmin(admin.ModelAdmin):
    list_display = ["title", "article_datetime"]
    list_display_links = ["title"]
    list_filter = ('article_datetime',)
    date_hierarchy = 'article_datetime'
    # ordering = ('-article_datetime',)
    search_fields = ["title", "article_author"]
    prepopulated_fields = {'slug': ('title',)}

    class Meta:
        model = RozanovArticle


class StoryAdmin(admin.ModelAdmin):
    list_display = ["title", "category", "story_datetime", "story_author"]
    list_display_links = ["title"]
    list_filter = ('story_datetime',)
    date_hierarchy = 'story_datetime'
    # ordering = ('-story_datetime',)
    search_fields = ["title", "story_author"]
    prepopulated_fields = {'slug': ('title',)}

    class Meta:
        model = Story


class ArticleCategoryAdmin(admin.ModelAdmin):
    list_display = ["title", "id"]
    list_display_links = ["title"]
    prepopulated_fields = {'slug': ('title',)}

    class Meta:
        model = ArticleCategory


class VideoMomentAdmin(admin.ModelAdmin):
    list_display = ["title", "video_datetime"]
    list_display_links = ["title"]

    class Meta:
        model = VideoMoment


admin.site.register(ArticleGuide, ArticleGuideAdmin)
admin.site.register(RozanovArticle, RozanovArticleAdmin)
admin.site.register(Story, StoryAdmin)
admin.site.register(ArticleCategory, ArticleCategoryAdmin)
admin.site.register(VideoMoment, VideoMomentAdmin)
