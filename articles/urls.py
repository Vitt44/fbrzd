# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from .views import (CityPage, GuidePage, RozanovList, RozanovPage, News,
                    ArticlePage, Video, GoldPath, success_call, FansPage,
                    get_restart, load_articles, load_articles_en, ch_story,
                    load_fifa, load_fifa_en)

urlpatterns = [
    url(r'^$', News.as_view(), name="stories"),
    url(r'^en/$', News.as_view(template_name='articles/stories/story_list_en.html', header='WORLD CUP NEWS', ajax_url='/load_articles_en/'), name="stories_en"),
    #url(r'^ch/$', News.as_view(template_name='articles/stories/story_list_ch.html', header='冠軍新聞'), name="stories_ch"),
    url(r'^ch/$', ch_story, name="stories_ch"),
    url(r'^restart/$', get_restart, name="get_restart"),
    url(r'^success/$', success_call, name="success_call"),
    url(r'^load_articles/$', load_articles, name="load_articles"),
    url(r'^load_articles_en/$', load_articles_en, name="load_articles_en"),
    url(r'^load_fifa/$', load_fifa, name="load_fifa"),
    url(r'^load_fifa_en/$', load_fifa_en, name="load_fifa_en"),
    url(r'^video/$', Video.as_view(), name="video"),
    url(r'^video/en/$', Video.as_view(template_name='videos/video_en.html'), name="video_en"),
    url(r'^video/ch/$', Video.as_view(template_name='videos/video_ch.html'), name="video_ch"),
    url(r'^video/path-to-gold/$', GoldPath.as_view(), name="gold"),
    url(r'^video/path-to-gold/en/$', GoldPath.as_view(template_name='videos/path_to_gold_en.html'), name="gold_en"),
    url(r'^video/path-to-gold/ch/$', GoldPath.as_view(template_name='videos/path_to_gold_ch.html'), name="gold_ch"),
    url(r'^fifa/$', News.as_view(id=2, header='ИСТОРИЯ ТУРНИРОВ FIFA', ajax_url='/load_fifa/'), name="fifa"),
    url(r'^fifa/en/$', News.as_view(template_name='articles/stories/story_list_en.html', id=2, header='HISTORY OF FIFA', ajax_url='/load_fifa_en/'), name="fifa_en"),
    url(r'^fifa/ch/$', News.as_view(template_name='articles/stories/story_list_ch.html', id=2, header='國際足聯比賽的歷史'), name="fifa_ch"),
    url(r'^fans/$', FansPage.as_view(), name="fans"),
    url(r'^fans/en/$', FansPage.as_view(template_name='for_fans/fans_en.html'), name="fans_en"),
    url(r'^fans/ch/$', FansPage.as_view(template_name='for_fans/fans_ch.html'), name="fans_ch"),
    # url(r'^fans_old/$', News.as_view(id=3, header='РЖД БОЛЕЛЬЩИКАМ'), name="fans_old"),
    # url(r'^fans_old/en/$', News.as_view(template_name='articles/stories/story_list_en.html', id=3, header='RUSSIAN RAILWAYS FOR THE FANS'), name="fans_old_en"),
    # url(r'^fans_old/ch/$', News.as_view(template_name='articles/stories/story_list_ch.html', id=3, header='俄羅斯鐵路的球迷'), name="fans_old_ch"),
    url(r'^article/(?P<slug>[-\w]+)/$', ArticlePage.as_view(), name="article_page"),
    url(r'^article/(?P<slug>[-\w]+)/en/$', ArticlePage.as_view(template_name='articles/stories/story_page_en.html'), name="article_page_en"),
    url(r'^article/(?P<slug>[-\w]+)/ch/$', ArticlePage.as_view(template_name='articles/stories/story_page_ch.html'), name="article_page_ch"),
    url(r'^rozanov/$', RozanovList.as_view(), name="rozanov_page"),
    url(r'^rozanov/en/$', RozanovList.as_view(template_name='articles/rozanov/rozanov_page_en.html'), name="rozanov_page_en"),
    url(r'^rozanov/ch/$', RozanovList.as_view(template_name='articles/rozanov/rozanov_page_ch.html'), name="rozanov_page_ch"),
    url(r'^rozanov/(?P<slug>[-\w]+)/$', RozanovPage.as_view(), name="rozanov_article"),
    url(r'^rozanov/(?P<slug>[-\w]+)/en/$', RozanovPage.as_view(template_name='articles/rozanov/rozanov_article_en.html'), name="rozanov_article_en"),
    url(r'^rozanov/(?P<slug>[-\w]+)/ch/$', RozanovPage.as_view(template_name='articles/rozanov/rozanov_article_ch.html'), name="rozanov_article_ch"),
    url(r'^guide/(?P<slug>[-\w]+)/$', CityPage.as_view(), name="city_page"),
    url(r'^guide/(?P<slug>[-\w]+)/en/$', CityPage.as_view(template_name='articles/guide/city_page_en.html'), name="city_page_en"),
    url(r'^guide/(?P<slug>[-\w]+)/ch/$', CityPage.as_view(template_name='articles/guide/city_page_ch.html'), name="city_page_ch"),
    url(r'^guide/(?P<city>[-\w]+)/(?P<slug>[-\w]+)/$', GuidePage.as_view(), name="guide_page"),
    url(r'^guide/(?P<city>[-\w]+)/(?P<slug>[-\w]+)/en/$', GuidePage.as_view(template_name='articles/guide/guide_page_en.html'), name="guide_page_en"),
    url(r'^guide/(?P<city>[-\w]+)/(?P<slug>[-\w]+)/ch/$', GuidePage.as_view(template_name='articles/guide/guide_page_ch.html'), name="guide_page_ch"),
]
