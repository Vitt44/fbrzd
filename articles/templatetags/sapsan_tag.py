import os
from django import template

register = template.Library()
# Wi-Fi top element
@register.inclusion_tag('sapsan.html')
def get_sapsan():
    wifi = ''
    online = ''
    sapsan = ''
    if os.path.exists("is_wifi.txt"):
         wifi += '1'
    if os.path.exists("is_online.py"):
         online += '1'
    if os.path.exists("is_sapsan.txt"):
         sapsan += '1'
    return {'wifi': wifi, 'online': online, 'sapsan': sapsan}
