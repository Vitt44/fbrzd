import os
from django import template

register = template.Library()
# Wi-Fi top element
@register.inclusion_tag('wifi.html')
def get_wifi():
    wifi = ''
    online = ''
    if os.path.exists("is_wifi.txt"):
         wifi += '1'
    if os.path.exists("is_online.py"):
         online += '1'
    return {'wifi': wifi, 'online': online}


@register.inclusion_tag('wifi_en.html')
def get_wifi_en():
    wifi = ''
    online = ''
    if os.path.exists("is_wifi.txt"):
         wifi += '1'
    if os.path.exists("is_online.py"):
         online += '1'
    return {'wifi': wifi, 'online': online}


@register.inclusion_tag('wifi_ch.html')
def get_wifi_ch():
    wifi = ''
    online = ''
    if os.path.exists("is_wifi.txt"):
         wifi += '1'
    if os.path.exists("is_online.py"):
         online += '1'
    return {'wifi': wifi, 'online': online}
